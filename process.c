#include <stdlib.h>
#include "process.h"

/* createProcess: allocates space for a new Process */
struct Process *createProcess(void) {
	return (struct Process *) malloc(sizeof(struct Process));
}

/* processCmp: comparison function for qsort */
int processCmp(const void *v1, const void *v2) {
	struct Process **p1 = (struct Process **) v1;
	struct Process **p2 = (struct Process **) v2;

	return (*p1)->pid - (*p2)->pid;
}
