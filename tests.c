#include <stdio.h>
#include <string.h>
#include "process.h"
#include "tree.h"
#include "util.h"
#include "parse_process.h"

int main() {
	
	// TEST process.c
	printf("/////////////////\n");
	printf("Testing process.c\n");
	
	struct Process *pp = createProcess();
	pp->pid = 1;
	pp->ppid = 2;
	pp->comm = "Some sort of command";
	
	printf("Expected: %d, %d, %s\n", 1, 2, "Some sort of command");
	printf("Actual  : %d, %d, %s\n", pp->pid, pp->ppid, pp->comm);
	
	printf("\n");
	
	
	// TEST tree.c
	printf("//////////////\n");
	printf("Testing tree.c\n");
	
	struct Process *p1 = createProcess();
	p1->pid = 1;
	p1->ppid = 0;
	p1->comm = "CMD1";
	
	struct Process *p2 = createProcess();
	p2->pid = 2;
	p2->ppid = 1;
	p2->comm = "CMD2";
	
	struct Process *p3 = createProcess();
	p3->pid = 3;
	p3->ppid = 1;
	p3->comm = "CMD3";
	
	struct Process *p4 = createProcess();
	p4->pid = 4;
	p4->ppid = 2;
	p4->comm = "CMD4";
	
	struct TreeNode *root;
	root = NULL;
	
	root = addNode(root, p1);
	root = addNode(root, p2);
	root = addNode(root, p3);
	root = addNode(root, p4);
	
	printf("Expected:\n");
	printf("1 CMD1\n");
	printf("  2 CMD2\n");
	printf("    4 CMD4\n");
	printf("  3 CMD3\n");
	
	printf("\nActual:\n");
	printTree(root, 0);
	
	printf("\n");
	
	
	// TEST util.c
	printf("//////////////\n");
	printf("Testing util.c\n");
	
	char *string = "I am a string.";
	char *copy = stringCopy(string);
	if(strcmp(string, copy) != 0)
		printf("stringCopy failed.");
	else
		printf("stringCopy passed.");
		
	printf("\n");
	
	char *newline = "New line\n";
	char *nonewline = stripNewline(newline);
	if (strstr(nonewline, "\n") != NULL)
		printf("stripNewline failed.");
	else
		printf("stripNewline passed.");

	printf("\n\n");
	
	
	// TEST parse_process.c
	printf("///////////////////////\n");
	printf("Testing parse_process.c\n");
	printf("Parse the first 20 processes\n");
	
	FILE *fp = popen("ps -e -l", "r");
	int firstRow = 1;
	char buffer[2048];
	struct Process *processes[20];
	
	int num_p = 0;
	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		if (firstRow == 0) {
			processes[num_p] = parseProcess(buffer);
			num_p++;
		} else {
			int error = parseProcessSetup(buffer);
			if (error < 0) {
				printf("parseProcessSetup failed.\n");
				return -1;
			}
			firstRow = 0;
		}
	}
	pclose(fp);
	
	// Print out the first 20 processes, making sure the pid, ppid, and comm
	//	were parsed.
	int i;
	for (i = 0; i < 20; i++) {
		printf("%d, %d, %s\n", processes[i]->ppid, processes[i]->pid, processes[i]->comm);
	}
	
	return 0;
}
