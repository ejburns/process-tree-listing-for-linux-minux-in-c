#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "util.h"
#include "tree.h"
#include "process.h"
#include "parse_process.h"

// This const is used for allocating the array off processes
//	that will be sorted before being put in the tree.
//	This is required for Minix, since the processes are not listed
//	in order.
const int MAX_PROCESSES = 1000;

/*
 * MAIN
 */
int main(int argc, char *argv[]) {
	
	// create a file out of the ps command
	FILE *fp = popen("ps -e -l", "r");
	
	// use first row for setup
	int firstRow = 1;
	
	// Array for holding processes to be sorted
	struct Process *processes[MAX_PROCESSES];
	
	// 2kb buffer for reading each line
	char buffer[2048];
	
	// loop through and get each line
	int num_p = 0;
	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		
		if (firstRow == 0) {
			// add the process to the array
			processes[num_p] = parseProcess(buffer);
			num_p++;
			
		// do parsing setup for the first row
		} else {
			int error = parseProcessSetup(buffer);

			// if the locations were not found, bail
			//	something went wrong.
			if (error < 0) {
				printf("Error processing columns.\n");
				return -1;
			}
			
			firstRow = 0;
		}
	}
	
	// close the file
	pclose(fp);
	
	// sort the processes
	qsort(processes, num_p, sizeof(struct Process *), processCmp);
	
	// set up the tree
	struct TreeNode *root;
	root = NULL;
	
	int i;
	for (i = 0; i < num_p; i++) {
		root = addNode(root, processes[i]);
	}
	
	// print the process tree
	printTree(root, 0);
	
	return 0;
}
