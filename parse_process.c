#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "parse_process.h"
#include "process.h"
#include "util.h"

/* token locations of pid, ppid, and cmd, given
 * the passed PS command. */

int pid_loc = -1,
	ppid_loc = -1,
	recv_loc = -1,
	cmd_loc = -1;

/* parseProcessSetup: determines the locations of pid,
 * ppid, and cmd given the first line. if it can't find them,
 * it returns a negative number, 0 or positive if all is well.
 */
int parseProcessSetup(char *firstline) {
	int cur_loc = 0;
	
	// break the line into tokens and compare each
	// to the column headers for pid, ppid, and cmd
	// save the location of each for parsing.
	char *tokens = strtok(firstline, " ");
	
	while (tokens != NULL) {
		
		if (strcmp(tokens, "PID") == 0) {
			pid_loc = cur_loc;
		} else if (strcmp(tokens, "PPID") == 0) {
			ppid_loc = cur_loc;
		} else if (strcmp(tokens, "RECV") == 0) {
			recv_loc = cur_loc;
		} else if (strcmp(tokens, "CMD\n") == 0) {
			cmd_loc = cur_loc;
		}
		
		// increment the column counter
		cur_loc++;
		
		// get the next token
		tokens = strtok(NULL, " ");
	}
	
	// If any of the locations were not set, return in error state.
	if (pid_loc == -1 || ppid_loc == -1 || cmd_loc == -1)
		return -1;
	
	return 0;
}

/* parseProcess: reads in the passed buffer line and extracts
 * the pid, ppid, and cmd based on the column locations
 * determined in parseProcessSetup. Handles both Linux and Minix.
 * 
 * returns - a pointer to a Process */
struct Process *parseProcess(char *buffer) {
	
	int pid, ppid;
	char* comm;
	
	// break the line into tokens and pick out
	// the PID, PPID, and CMD values
	int cur_loc = 0;
	char *tokens = strtok(buffer, " ");
	while(tokens != NULL) {
		
		if (cur_loc == pid_loc) {
			// In Minix, some PID's are surrounded in ()
			char *token = stringCopy(tokens);
			removeChar(token, '(');
			removeChar(token, ')');
			pid = atoi(token);
		} else if (cur_loc == ppid_loc) {
			ppid = atoi(tokens);
		} else if (cur_loc == recv_loc) {
			// In Minix RECV has 2 tokens, skip one
			if (strstr(tokens, "(") != NULL) tokens = strtok(NULL, " ");
			// In Minix RECV has no tokens, reuse this token
			if (strstr(tokens, "?") != NULL) {
				cur_loc++;
				continue;
			}
		} else if (cur_loc == cmd_loc) {
			// strip newline to prevent formatting issues
			comm = stripNewline(tokens);
		} else if (cur_loc > cmd_loc) {
			// In Minix, CMD can have many spaces
			strcat(comm, stripNewline(tokens));
		}
		
		// increment the column counter
		cur_loc++;
		
		// get the next token
		tokens = strtok(NULL, " ");
	}

	// create a new process
	struct Process *p = createProcess();
	p->pid = pid;
	p->ppid = ppid;
	p->comm = stringCopy(comm);
	
	return p;
}
