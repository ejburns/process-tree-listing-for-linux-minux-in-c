struct Process {
	int pid;		/* process id */
	int ppid;		/* parent process id */
	char *comm;		/* points to command */
};

struct Process *createProcess(void);
int processCmp(const void *, const void *);
