#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "process.h"

/* createNode: allocates memory for a new TreeNode */
struct TreeNode *createNode(void) {
	return (struct TreeNode *) malloc(sizeof(struct TreeNode));
}

/* addNode: add a node with the passed process as a child or sibling
 * 
 * This code was adapted from section 6.5 of C Programming Language
 */
struct TreeNode *addNode(struct TreeNode *node, 
						 struct Process *process) {
	if (node == NULL) {
		node = createNode();
		node->process = process;
		node->firstChild = node->nextSibling = NULL;
	} else if (node->process->pid == process->ppid) { //is node parent
		node->firstChild = addNode(node->firstChild, process);
	} else if (node->process->ppid == process->ppid) { // is node sibling
		node->nextSibling = addNode(node->nextSibling, process);
	} else { // is node uncle, grandparent
		addNode(node->nextSibling, process);
		addNode(node->firstChild, process);
	}
	return node;
}

/* printTree: prints the tree as a directory-like structure */
void printTree(struct TreeNode *node, int depth) {
	if (node != NULL) {
		
		int i;
		for (i=0; i < depth; i++)
			printf("  ");
		
		printf("%d %s\n", node->process->pid, node->process->comm);
		printTree(node->firstChild, depth+1);
		printTree(node->nextSibling, depth);
	}
}
