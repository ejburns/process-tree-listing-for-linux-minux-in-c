#include <string.h>
#include <stdlib.h>
#include "util.h"

/* stringCopy: allocate space for the passed string and return a new copy of it */
char *stringCopy(const char *string) {
	char *copy = malloc(strlen(string) +1);
	strcpy(copy, string);
	return copy;
}

/* stripNewline: strips newline characters from strings */
char *stripNewline(char *string) {	
	if (strstr(string, "\n") != NULL) {
		char *str = malloc(strlen(string));
		str = strncpy(str, string, strlen(string) -1);
		return str;
	}
	return string;
}

/* removeChar: removes a character from a string */
void removeChar(char *string, char letter) {
	int i;
	for (i = 0; i < strlen(string); i++)
		if (string[i] == letter)
			strcpy(string + i, string + i + 1);
}
