#CC=/usr/pkg/bin/gcc
CC=/usr/bin/gcc
CFLAGS= -g -Wall -I.
LINKFLAGS= -g -Wall

all: process_tree tests

process_tree: main.o parse_process.o process.o tree.o util.o
	$(CC) $(LINKFLAGS) -o process_tree main.o parse_process.o process.o tree.o util.o

tests: tests.o parse_process.o process.o tree.o util.o
	$(CC) $(LINKFLAGS) -o tests tests.o parse_process.o process.o tree.o util.o

main.o: main.c util.h tree.h parse_process.h
	$(CC) $(CFLAGS) -c main.c -o main.o

parse_process.o: parse_process.c parse_process.h process.h util.h
	$(CC) $(CFLAGS) -c parse_process.c -o parse_process.o

process.o: process.c process.h
	$(CC) $(CFLAGS) -c process.c -o process.o

tree.o: tree.c tree.h process.h
	$(CC) $(CFLAGS) -c tree.c -o tree.o

util.o: util.c util.h
	$(CC) $(CFLAGS) -c util.c -o util.o

tests.o: tests.c
	$(CC) $(CFLAGS) -c tests.c

clean:
	rm -f *.o process_tree tests
