Comp 374
Homework 1
Eric Burns

NOTE: I realize there are a lot of lines of code. I had trouble getting
this to work in both Minix and Linux, without implementing quite a few
special case catches for Minix's output. Also, it required me to
completely refactor my Linux code. (As you will see in the commit
history, the Linux version was only about 150 loc.)

Linux:

1. Run "make".
2a. Run "./process_tree" for the process tree.
2b. Run "./tests" to run the unit tests.

Minix:

1. Comment out the CC variable for Linux and uncomment the Minix one in
   the Makefile.
2. Run "make".
3a. Run "./process_tree" for the process tree.
3b. Run "./tests" to run the unit tests.
