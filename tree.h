struct TreeNode {
	struct Process *process;		/* points to the process */
	struct TreeNode *firstChild;	/* points to the first child */
	struct TreeNode *nextSibling;	/* points to the next sibling */
};

struct TreeNode *createNode(void);
struct TreeNode *addNode(struct TreeNode *, struct Process *);
void printTree(struct TreeNode *, int);
